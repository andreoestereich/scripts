#!/bin/bash

function BibFile () {
    list=$(grep '^@' $1)
    for line in $list
    do
        tag=$(echo $line | sed 's/.*{//;s/,//')
        printf "$tag\t$FILE\t/^$line\$/;\"\tb\n" | sed 's/\r//g' >> tags
    done
}

function TexFile () {
    list=$(grep '\label{' $1)
    for line in $list
    do
        tag=$(echo $line | sed 's/.*label{//;s/}.*//;s/\r//g')
        printf "$tag\t$FILE\t/\\label{$tag}/;\"\tl\n" >> tags
    done
}

echo '!_TAG_FILE_FORMAT	2	/extended format; --format=1 will not append ;" to lines/' > tags
echo '!_TAG_FILE_SORTED	0	/0=unsorted, 1=sorted, 2=foldcase/' >> tags
echo '!_TAG_OUTPUT_MODE	u-ctags	/u-ctags or e-ctags/' >> tags
echo '!_TAG_PROGRAM_AUTHOR	andrelo	//' >> tags

for FILE in `find . -type f`
do
    case "$FILE" in
        *\.tex) TexFile $FILE ;;
        *\.bib) BibFile $FILE ;;
    esac
done

