#!/bin/sh

statfile=~/.cache/ygofmstat

test -f $statfile || echo -e "FUSIONS=0\nEFFATK=0\nTRAP=0\nMAGIC=0\nEQUIP=0" > $statfile

source $statfile

choice=$( echo -e 'Fusion\nEffAtk\nEquip\nMagic\nTrap\nEnd' | dmenu )

case $choice in
    Fusion*) count=$(seq 6 | dmenu);FUSIONS=$((FUSIONS+count));;
    EffAtk*) ((EFFATK++));;
    Equip*) count=$(seq 6 | dmenu);EQUIP=$((EQUIP+count));;
    Magic*) ((MAGIC++));;
    Trap*) ((TRAP++));;
    End*) FUSIONS=0;EFFATK=0;TRAP=0;MAGIC=0;EQUIP=0;;
    *) ;;
esac

if [ "$choice" == "End" ]
then
    dunstify -C 12541
else
    dunstify -r 12541 -t 0 "Fusions=$FUSIONS" "Effatk=$EFFATK\nTrap=$TRAP\nMagic=$MAGIC\nEquip=$EQUIP"
fi


echo -e "FUSIONS=$FUSIONS\nEFFATK=$EFFATK\nTRAP=$TRAP\nMAGIC=$MAGIC\nEQUIP=$EQUIP" > $statfile
