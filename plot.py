#!/usr/bin/python3

import matplotlib.pyplot as plt
import numpy as np

from sys import argv
from os.path import isfile

#plt.rc('text', usetex=True)
#plt.rc('font', family='serif')

if len(argv) > 2:

    ls="-"
    columns = []
    data_files = []
    hLines = []
    vLines = []
    singleColor = False
    histPlot = False
    for argument in argv[1:]:
        if argument.isdigit():
            columns.append(int(argument))
        elif isfile(argument):
            data_files.append(argument)
        elif (argument.find("-ls=") != -1):
            ls = argument.split("=")[1]
        elif (argument.find("-hl=") != -1):
            hLines.append(float(argument.split("=")[1]))
        elif (argument.find("-vl=") != -1):
            vLines.append(float(argument.split("=")[1]))
        elif (argument.find("-sc") != -1):
            singleColor = True
        elif (argument.find("-hist") != -1):
            histPlot = True

    if histPlot:
        if len(columns) <= 1 and len(data_files) == 1:
            if len(columns) == 0:
                columns.append(0)

            arr = np.loadtxt(data_files[0],usecols=columns)
            plt.hist(arr, bins=20, density=False)
            plt.show()

        else:
            print("Histogram plot needs 1 file and at most 1 column")

    #just a sanity check
    elif len(columns) > 0 and len(data_files) > 0:
        for data in data_files:
            arr = np.loadtxt(data)
            arr = arr.transpose()

            for col in columns:
                if col < len(arr):
                    if singleColor:
                        plt.plot(arr[0],arr[col],lw=1.5,ls=ls, marker="o",color="blue")
                    else:
                        plt.plot(arr[0],arr[col],lw=1.5,marker="o",ls=ls, label="%i_"%(col)+data)
                    #

        if len(hLines) > 0:
            for lineX in hLines:
                plt.axhline(lineX,ls='dotted',c='black')

        if len(vLines) > 0:
            for lineY in vLines:
                plt.axvline(lineY,ls='dotted',c='black')

        plt.legend()
        plt.show()

    else:
        print("No files or columns you idiot!!!\n")
